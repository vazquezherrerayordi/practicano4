package ito.poo.clases;

import java.util.ArrayList;

public class Prenda {
  
	
	private int modelo;
	private String tela;
	private float costoProduccion;
	private ArrayList<String> genero;
	private ArrayList<String>temporada;
	private Lote lotes;
	public Prenda(int modelo, String tela, float costoProduccion, Lote lotes) {
		super();
		this.modelo = modelo;
		this.tela = tela;
		this.costoProduccion = costoProduccion;
		this.lotes = lotes;
		this.genero=new ArrayList<String>();
		this.temporada=new ArrayList<String>();
	}
	 public void addNvoLote (Lote lote) {
		 return;
	}
	public Lote recuperaLote(int i) {
		return null;
	}
	public int getModelo() {
		return modelo;
	}
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}
	public String getTela() {
		return tela;
	}
	public void setTela(String tela) {
		this.tela = tela;
	}
	public Lote getLotes() {
		return lotes;
	}
	public void setLotes(Lote lotes) {
		this.lotes = lotes;
	}
	public float getCostoProduccion() {
		return costoProduccion;
	}
	public ArrayList<String> getGenero() {
		return genero;
	}
	public ArrayList<String> getTemporada() {
		return temporada;
	}
	@Override
	public String toString() {
		return "Prenda [modelo=" + modelo + ", tela=" + tela + ", costoProduccion=" + costoProduccion + ", genero="
				+ genero + ", temporada=" + temporada + ", lotes=" + lotes + "]";
	}
}
