package ito.poo.clases;

import java.time.LocalDate;

public class Lote {

	private int numLote;
	 private String numPiezas;
	 private LocalDate fecha;
	 public Lote(int numLote, String numPiezas, LocalDate fecha) {
		super();
		this.numLote = numLote;
		this.numPiezas = numPiezas;
		this.fecha = fecha;
	}
	 public float costoProduccion() {
		 return 0f;
	 }
	 public float montoRecuperacionPorLote() {
		 return 0f;
	 }
	 public float montoRecuperacionPorPieza() {
		 return 0f;
	 }
	public int getNumLote() {
		return numLote;
	}
	public void setNumLote(int numLote) {
		this.numLote = numLote;
	}
	public String getNumPiezas() {
		return numPiezas;
	}
	public void setNumPiezas(String numPiezas) {
		this.numPiezas = numPiezas;
	}
	public LocalDate getFecha() {
		return fecha;
	}
	@Override
	public String toString() {
		return "Lote [numLote=" + numLote + ", numPiezas=" + numPiezas + ", fecha=" + fecha + "]";
	}
}
