package ito.poo.clases;

import java.util.ArrayList;

public class Zapato {
	
	private int clave;
    private String material;
    private String troquel;
    private int cantidadProducida;
    private ArrayList<String> colores;
    public Zapato(int clave, String material, String troquel, int cantidadProducida) {
		super();
		this.clave = clave;
		this.material = material;
		this.troquel = troquel;
		this.cantidadProducida = cantidadProducida;
		this.colores= new ArrayList<String>();
	}
    public void agregaColor(String color) {
    	
    }
    public float costoXlote(float costoXUnidad) {
    	return 0F;
    }
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getTroquel() {
		return troquel;
	}
	public void setTroquel(String troquel) {
		this.troquel = troquel;
	}
	public int getCantidadProducida() {
		return cantidadProducida;
	}
	public void setCantidadProducida(int cantidadProducida) {
		this.cantidadProducida = cantidadProducida;
	}
	public int getClave() {
		return clave;
	}
	public ArrayList<String> getColores() {
		return colores;
	}
	@Override
	public String toString() {
		return "Zapato [clave=" + clave + ", material=" + material + ", troquel=" + troquel + ", cantidadProducida="
				+ cantidadProducida + ", colores=" + colores + "]";
	}
}
     