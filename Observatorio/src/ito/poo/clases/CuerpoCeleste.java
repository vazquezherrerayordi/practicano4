package ito.poo.clases;

public class CuerpoCeleste {

	public CuerpoCeleste(String nombre, Ubicacion localizaciones, String composicion) {
		super();
		this.nombre = nombre;
		this.localizaciones = localizaciones;
		this.composicion = composicion;
	}

	private String nombre;
	private Ubicacion localizaciones;
	private String composicion;
	
	public float desplazamiento (int j , int i) {
		return 0f;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Ubicacion getLocalizaciones() {
		return localizaciones;
	}

	public void setLocalizaciones(Ubicacion localizaciones) {
		this.localizaciones = localizaciones;
	}

	public String getComposicion() {
		return composicion;
	}

	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}

	@Override
	public String toString() {
		return "CuerpoCeleste [nombre=" + nombre + ", localizaciones=" + localizaciones + ", composicion=" + composicion
				+ "]";
	}
}
