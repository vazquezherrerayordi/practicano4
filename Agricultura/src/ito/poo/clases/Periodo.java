package ito.poo.clases;

public class Periodo {

	public Periodo(String tiempoCosecha, float cantCosechaxtiempo) {
		super();
		this.tiempoCosecha = tiempoCosecha;
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}
	private String tiempoCosecha;
	private float cantCosechaxtiempo;
	
	public float costoPeriodo() {
		return 0f;
	}
	public float gananciaEstimada() {
		return 0f;
	}
	public String getTiempoCosecha() {
		return tiempoCosecha;
	}
	public void setTiempoCosecha(String tiempoCosecha) {
		this.tiempoCosecha = tiempoCosecha;
	}
	public float getCantCosechaxtiempo() {
		return cantCosechaxtiempo;
	}
	public void setCantCosechaxtiempo(float cantCosechaxtiempo) {
		this.cantCosechaxtiempo = cantCosechaxtiempo;
	}
	@Override
	public String toString() {
		return "Periodo [tiempoCosecha=" + tiempoCosecha + ", cantCosechaxtiempo=" + cantCosechaxtiempo + "]";
	}
}
