package ito.poo.clases;

import java.util.ArrayList;

public class Fruta {

	private String fruta;
	private float extension;
	private float costoPromedio;
	private float VentaProm;
	private ArrayList<Periodo>periodos;
	public Fruta(String fruta, float extension, float costoPromedio, float ventaProm) {
		super();
		this.fruta = fruta;
		this.extension = extension;
		this.costoPromedio = costoPromedio;
		VentaProm = ventaProm;
	}
	public void agregarPeriodo(Periodo p){
		return;
	}
	public boolean eliminarPeriodo(int i) {
		return false;
	}
	public String getFruta() {
		return fruta;
	}
	public void setFruta(String fruta) {
		this.fruta = fruta;
	}
	public float getExtension() {
		return extension;
	}
	public void setExtension(float extension) {
		this.extension = extension;
	}
	public float getCostoPromedio() {
		return costoPromedio;
	}
	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}
	public float getVentaProm() {
		return VentaProm;
	}
	public void setVentaProm(float ventaProm) {
		VentaProm = ventaProm;
	}
	public ArrayList<Periodo> getPeriodos() {
		return periodos;
	}
	@Override
	public String toString() {
		return "Fruta [fruta=" + fruta + ", extension=" + extension + ", costoPromedio=" + costoPromedio
				+ ", VentaProm=" + VentaProm + ", periodos=" + periodos + "]";
	}
}
